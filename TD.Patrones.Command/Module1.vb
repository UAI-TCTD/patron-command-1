﻿Module Module1

    Sub Main()

        Dim empresa As New EmpresaInvoker


        Dim producto As New ProductoReceiver
        producto.Cantidad = 100

        Dim ordenAlta As New AltaStockCommand(producto, 10)
        empresa.TomarOrden(ordenAlta)
        Dim ordenbaja As New BajaStockCommand(producto, 50)
        empresa.TomarOrden(ordenbaja)

        empresa.ProcesarOrdenes()
        Console.Write(String.Format("Total stock es {0}", producto.Cantidad))
        Console.ReadKey()


    End Sub

End Module
