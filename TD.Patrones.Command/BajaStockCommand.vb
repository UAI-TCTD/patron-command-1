﻿Public Class BajaStockCommand
    Inherits OrdenCommand

    Public Sub New(producto As ProductoReceiver, cantidad As Double)
        MyBase.New(producto, cantidad)
    End Sub


    Public Overrides Sub Ejecutar()
        _producto.RestarStock(_cantidad)
    End Sub
End Class
