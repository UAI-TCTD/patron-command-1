﻿Public Class EmpresaInvoker

    Private ordenes As New List(Of OrdenCommand)


    Public Sub TomarOrden(cmd As OrdenCommand)
        ordenes.Add(cmd)
    End Sub

    Public Sub ProcesarOrdenes()
        For Each orden In ordenes
            orden.Ejecutar()
        Next
        ordenes.Clear()
    End Sub
End Class
