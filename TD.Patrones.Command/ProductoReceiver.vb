﻿Public Class ProductoReceiver
    Public Property Cantidad As Double
    Public Property Nombre As String
    Public Sub RestarStock(cant As Double)
        Cantidad = Cantidad - cant
        Console.WriteLine(String.Format("Quitando {0} unidades", cant))
    End Sub
    Public Sub SumarStock(cant As Double)
        Cantidad = Cantidad + cant
        Console.WriteLine(String.Format("Agregando {0} unidades", cant))
    End Sub
End Class
