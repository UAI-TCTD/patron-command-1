﻿Public MustInherit Class OrdenCommand
    MustOverride Sub Ejecutar()
    Protected _producto As ProductoReceiver
    Protected _cantidad As Double
    Public Sub New(producto As ProductoReceiver, cantidad As Double)
        _producto = producto
        _cantidad = cantidad
    End Sub
End Class
