﻿Public Class AltaStockCommand
    Inherits OrdenCommand
    Public Sub New(producto As ProductoReceiver, cantidad As Double)
        MyBase.New(producto, cantidad)
    End Sub
    Public Overrides Sub Ejecutar()
        Me._producto.SumarStock(_cantidad)
    End Sub
End Class
